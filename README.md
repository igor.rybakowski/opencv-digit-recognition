# OpenCV digit recognition

For Python implementation check python branch


## Requirements

OpenCV 3

g++

## Usage

First compile train_model.cpp 

```
g++ train_model.cpp -o model_training `pkg-config --cflags --libs opencv`

```

Then launch model_training with path to 'img/digits.png' as an argument

```
./model_training img/digits.png
```

Now your trained model.xml is ready

## Recognize digits

Compile the  main.cpp into executable

```
g++ main.cpp -o main `pkg-config --cflags --libs opencv`

```

Pass the .jpg/.png as argument for main

```
./main img.jpg
```