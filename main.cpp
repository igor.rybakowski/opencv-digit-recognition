#include <iostream>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include "opencv2/objdetect.hpp"
#include <opencv2/ml.hpp>

using namespace cv::ml;
using namespace cv;
using namespace std;

float affineFlags = WARP_INVERSE_MAP|INTER_LINEAR;

Size size(20,20);

HOGDescriptor hog(
    Size(20,20), //winSize
    Size(8,8), //blocksize
    Size(4,4), //blockStride,
    Size(8,8), //cellSize,
    9,   //nbins,
    1,   //derivAper,
    -1,  //winSigma,
    cv::HOGDescriptor::L2Hys, //histogramNormType,
    0.2, //L2HysThresh,
    0,   //gammal correction,
    64,  //nlevels=64
    1
);

Mat load_img(char *path);
void save_img(Mat& img);
Mat deskew(Mat& img);

auto main(int argc, char **argv) -> int {
    Ptr<SVM> model = SVM::load("./model.xml");
    Mat img_matrix = load_img(argv[1]);
    Mat img_deskewed = deskew(img_matrix);
    Mat testResponse;
    vector<float> descriptor;
    // save_img(img_deskewed);
    // img_deskewed = load_img("./img.jpg");
    hog.compute(img_deskewed, descriptor);
    model->predict(descriptor, testResponse);
    cout << "MACHINE SAYS ITS:  " << testResponse.at<float>(0,0) << endl;
}

Mat load_img(char *path) {
    Mat testImage;
    std::string image = path;
    testImage = imread(image.c_str());
    if (!testImage.data){
        cout<<"Error opening the image!";
    }
    cout<<"Successfully loaded image"<<endl;
    return testImage;
}

void save_img(Mat& img) {
    imwrite("./img.jpg", img);
}

Mat deskew(Mat& img) {
    Mat mSource_Bgr,mSource_Gray,mThreshold, Points, resized;
    mSource_Bgr= img;
    cvtColor(mSource_Bgr,mSource_Gray,COLOR_BGR2GRAY);
    threshold(mSource_Gray,mThreshold,100,255,THRESH_BINARY_INV);
    findNonZero(mThreshold,Points);
    Rect Min_Rect=boundingRect(Points);
    rectangle(mSource_Bgr,Min_Rect.tl(),Min_Rect.br(),Scalar(0,255,0),2);
    //imshow("img",mSource_Bgr);
    Mat cropped(mSource_Bgr, Min_Rect);
    resize(cropped, resized, size);
    return resized;
}
