#include <iostream>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include "opencv2/objdetect.hpp"
#include <opencv2/ml.hpp>

using namespace cv::ml;
using namespace cv;
using namespace std;


auto main(int argc, char **argv) -> int {
    //Mat img = imread(argv[0], cv::IMREAD_GRAYSCALE);
    Mat mSource_Bgr,mSource_Gray,mThreshold;
    mSource_Bgr= imread(argv[1],1);
    cvtColor(mSource_Bgr,mSource_Gray,COLOR_BGR2GRAY);
    threshold(mSource_Gray,mThreshold,100,255,THRESH_BINARY_INV);

    Mat Points;
    findNonZero(mThreshold,Points);
    Rect Min_Rect=boundingRect(Points);
    //rectangle(mSource_Bgr,Min_Rect.tl(),Min_Rect.br(),Scalar(0,255,0),2);
    //imshow("img",mSource_Bgr);

    Mat cropped(mSource_Bgr, Min_Rect);
    Mat resized;

    Size size(20,20);

    resize(cropped, resized, size);

    imwrite("./resized.jpg", resized);

    // cout << resized.size() << endl;
    // imshow("img",resized);
    // cv::waitKey( 0 );
    // cv::destroyWindow( "img" );

    return 0;
}